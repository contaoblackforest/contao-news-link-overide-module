News list Overide Link
=======================

This is an extension for Contao News.
You can overide the link in the module settings. 


System requirements
-------------------

 * Contao 3.3
 * Web server
 * PHP 5.3.2+ with GDlib, DOM, Phar and SOAP
 * MySQL 5.0.3+
