<?php
/**
 * FRAMEWORK
 *
 * Copyright (C) FRAMEWORK
 *
 * @package   Contao News
 * @file      OverideLink.php
 * @author    Sven Baumann <baumann.sv@gmail.com>
 * @author    Dominik Tomasi <dominik.tomasi@gmail.com>
 * @license   GNU/LGPL
 * @copyright Copyright 2014 owner
 */


namespace ContaoBlackforest\News\ModuleList;


use Contao\ModuleNewsList;


/**
 * Class OverideLink
 *
 * @package ContaoBlackforest\News\ModuleList
 */
class OverideLink extends ModuleNewsList
{

	/**
	 * URL cache array
	 *
	 * @var array
	 */
	private static $arrUrlCache = array();


	/**
	 * Display a wildcard in the back end from extended class
	 *
	 * @return string
	 */
	public function generate()
	{
		parent::generate();

		return parent::generate();
	}


	/**
	 * Generate the module from extended class
	 */
	protected function compile()
	{
		parent::compile();
	}


	/**
	 * Generate a URL and return it as string from extended class
	 * if the module has an overide link generate a url with this
	 *
	 * @param object
	 * @param boolean
	 *
	 * @return string
	 */
	protected function generateNewsUrl($objItem, $blnAddArchive = false)
	{
		if ($this->objModel->jumpToNews) {
			return $this->generateNewsUrlOveride($objItem, $blnAddArchive);
		}

		return parent::generateNewsUrl($objItem, $blnAddArchive);
	}

	/**
	 * Generate a URL and return it as string
	 *
	 * @param object
	 * @param boolean
	 *
	 * @return string
	 */
	protected function generateNewsUrlOveride($objItem, $blnAddArchive = false)
	{
		$strCacheKey = 'id_' . $objItem->id;

		// Load the URL from cache
		if (isset(self::$arrUrlCache[$strCacheKey])) {
			return self::$arrUrlCache[$strCacheKey];
		}

		// Initialize the cache
		self::$arrUrlCache[$strCacheKey] = null;

		switch ($objItem->source) {
			// Link to an external page
			case 'external':
				if (substr($objItem->url, 0, 7) == 'mailto:') {
					self::$arrUrlCache[$strCacheKey] = \String::encodeEmail($objItem->url);
				}
				else {
					self::$arrUrlCache[$strCacheKey] = ampersand($objItem->url);
				}
				break;

			// Link to an internal page
			case 'internal':
				if (($objTarget = $objItem->getRelated('jumpTo')) !== null) {
					self::$arrUrlCache[$strCacheKey] = ampersand($this->generateFrontendUrl($objTarget->row()));
				}
				break;

			// Link to an article
			case 'article':
				if (($objArticle = \ArticleModel::findByPk($objItem->articleId, array('eager' => true))) !== null && ($objPid = $objArticle->getRelated('pid')) !== null) {
					self::$arrUrlCache[$strCacheKey] = ampersand($this->generateFrontendUrl($objPid->row(), '/articles/' . ((!\Config::get('disableAlias') && $objArticle->alias != '') ? $objArticle->alias : $objArticle->id)));
				}
				break;
		}

		// Link to the overide page
		if (self::$arrUrlCache[$strCacheKey] === null) {
			$objPage = \PageModel::findByPk($this->objModel->jumpToNews);

			if ($objPage === null) {
				self::$arrUrlCache[$strCacheKey] = ampersand(\Environment::get('request'), true);
			}
			else {
				self::$arrUrlCache[$strCacheKey] = ampersand($this->generateFrontendUrl($objPage->row(), ((\Config::get('useAutoItem') && !\Config::get('disableAlias')) ? '/' : '/items/') . ((!\Config::get('disableAlias') && $objItem->alias != '') ? $objItem->alias : $objItem->id)));
			}

			// Add the current archive parameter (news archive)
			if ($blnAddArchive && \Input::get('month') != '') {
				self::$arrUrlCache[$strCacheKey] .= (\Config::get('disableAlias') ? '&amp;' : '?') . 'month=' . \Input::get('month');
			}
		}

		return self::$arrUrlCache[$strCacheKey];
	}
}