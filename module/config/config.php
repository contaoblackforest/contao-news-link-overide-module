<?php
/**
 * FRAMEWORK
 * 
 * Copyright (C) FRAMEWORK
 * 
 * @package   Contao News
 * @file      config.php
 * @author    Sven Baumann <baumann.sv@gmail.com>
 * @author    Dominik Tomasi <dominik.tomasi@gmail.com>
 * @license   GNU/LGPL
 * @copyright Copyright 2014 owner
 */


$GLOBALS['FE_MOD']['news']['newslist'] = '\ContaoBlackforest\News\ModuleList\OverideLink';
