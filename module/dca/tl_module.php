<?php
/**
 * FRAMEWORK
 * 
 * Copyright (C) FRAMEWORK
 * 
 * @package   Contao News
 * @file      tl_module.php
 * @author    Sven Baumann <baumann.sv@gmail.com>
 * @author    Dominik Tomasi <dominik.tomasi@gmail.com>
 * @license   GNU/LGPL
 * @copyright Copyright 2014 owner
 */


$strPaletteNewsList = &$GLOBALS['TL_DCA']['tl_module']['palettes']['newslist'];
$strPaletteNewsList = str_replace('news_archives', 'news_archives,jumpToNews', $strPaletteNewsList);

$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToNews'] = $GLOBALS['TL_DCA']['tl_module']['fields']['jumpTo'];
$GLOBALS['TL_DCA']['tl_module']['fields']['jumpToNews']['label'] = &$GLOBALS['TL_LANG']['tl_module']['jumpToNews'];